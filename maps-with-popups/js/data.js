var info = {
    belmont_1_2: {
        lot6: {
            community: 'Belmont',
            phase: 'II',
            address: '2825 Greger St. Oakdale, CA 95361',
            elevation: 'craftsman',
            lotNumber: '6',
            squareFeet: '2333',
            homeStyle: 'two-story',
            lotSize: '6501'    
        },
        lot7: {
            community: 'Belmont',
            phase: 'II',
            address: '2788 Mustang Dr. Oakdale, CA 95361',
            elevation: 'spanish',
            lotNumber: '7',
            squareFeet: '2333',
            homeStyle: 'two-story',
            lotSize: '6502'    
        },
        lot8: {
            community: 'Belmont',
            phase: 'II',
            address: '2776 Mustang Dr. Oakdale, CA 95361',
            elevation: 'plantation',
            lotNumber: '8',
            squareFeet: '2368',
            homeStyle: 'two-story',
            lotSize: '6042'    
        },
        lot16: {
            community: 'Belmont',
            phase: 'II',
            address: '628 Criolla Dr. Oakdale, CA 95361',
            elevation: 'craftsman',
            lotNumber: '16',
            squareFeet: '2368',
            homeStyle: 'two-story',
            lotSize: '6023'    
        },
        lot17: {
            community: 'Belmont',
            phase: 'II',
            address: '618 Criolla Dr. Oakdale, CA 95361',
            elevation: 'spanish',
            lotNumber: '17',
            squareFeet: '2368',
            homeStyle: 'two-story',
            lotSize: '6151'    
        }
    },
    belmont_3: {
        lot57: {
            community: 'Belmont',
            phase: 'III',
            address: '2351 Mustang Dr. Oakdale, CA 95361',
            elevation: 'spanish',
            lotNumber: '57',
            squareFeet: '2609',
            homeStyle: 'single-story',
            lotSize: '8210'    
        },
        lot60: {
            community: 'Belmont',
            phase: 'III',
            address: '2363 Mustang Dr. Oakdale, CA 95361',
            elevation: 'monterey',
            lotNumber: '60',
            squareFeet: '2291',
            homeStyle: 'single-story',
            lotSize: '7418'    
        },
        lot64: {
            community: 'Belmont',
            phase: 'III',
            address: '2485 Mustang Dr. Oakdale, CA 95361',
            elevation: 'monterey',
            lotNumber: '64',
            squareFeet: '2209',
            homeStyle: 'two-story',
            lotSize: '8009'    
        },
        lot87: {
            community: 'Belmont',
            phase: 'III',
            address: '2550 Mustang Dr. Oakdale, CA 95361',
            elevation: 'mediterranean',
            lotNumber: '87',
            squareFeet: '1919',
            homeStyle: 'single-story',
            lotSize: '7383'    
        },
        lot99: {
            community: 'Belmont',
            phase: 'III',
            address: '2360 Mustang Dr. Oakdale, CA 95361',
            elevation: 'craftsman',
            lotNumber: '99',
            squareFeet: '2291',
            homeStyle: 'single-story',
            lotSize: '6562'    
        },
        lot105: {
            community: 'Belmont',
            phase: 'III',
            address: '2280 Mustang Dr. Oakdale, CA 95361',
            elevation: 'spanish',
            lotNumber: '105',
            squareFeet: '2209',
            homeStyle: 'two-story',
            lotSize: '6888'    
        },
        lot145: {
            community: 'Belmont',
            phase: 'III',
            address: '2487 Greger St. Oakdale, CA 95361',
            elevation: 'craftsman',
            lotNumber: '145',
            squareFeet: '2055',
            homeStyle: 'two-story',
            lotSize: '6000'    
        },
        lot147: {
            community: 'Belmont',
            phase: 'III',
            address: '2535 Greger St. Oakdale, CA 95361',
            elevation: 'monterey',
            lotNumber: '147',
            squareFeet: '1919',
            homeStyle: 'single-story',
            lotSize: '6000'    
        }
    },
    belmont_5: {
        lot94: {
            "lotNumber":94,
            "phase": "V",
            "address":"710 Ranger St. Oakdale, CA 95361",
            "squareFeet":1960,
            "elevation":"monterey",
            "homeStyle":"single-story",
            "lotSize":"7874",
            "community":"Belmont"
        },
        lot95: {
            "lotNumber":95,
            "phase": "V",
            "address":"722 Ranger St. Oakdale, CA 95361",
            "squareFeet":2209,
            "elevation":"craftsman",
            "homeStyle":"two-story",
            "lotSize":"7044",
            "community":"Belmont"
        },
        lot96: {
            "lotNumber":96,
            "phase": "V",
            "address":"734 Ranger St. Oakdale, CA 95361",
            "squareFeet":2810,
            "elevation":"plantation",
            "homeStyle":"two-story",
            "lotSize":"8065",
            "community":"Belmont"
        },
        lot97: {
            "lotNumber":97,
            "phase": "V",
            "address":"746 Ranger St. Oakdale, CA 95361",
            "squareFeet":2209,
            "elevation":"spanish",
            "homeStyle":"two-story",
            "lotSize":"7276",
            "community":"Belmont"
        },
        lot98: {
            "lotNumber":98,
            "phase": "V",
            "address":"758 Ranger St. Oakdale, CA 95361",
            "squareFeet":1919,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot101: {
            "lotNumber":101,
            "phase": "V",
            "address":"791 Ranger St. Oakdale, CA 95361",
            "squareFeet":1919,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"6888",
            "community":"Belmont"
        },
        lot102: {
            "lotNumber":102,
            "phase": "V",
            "address":"773 Ranger St. Oakdale, CA 95361",
            "squareFeet":1754,
            "elevation":"spanish",
            "homeStyle":"single-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot103: {
            "lotNumber":103,
            "phase": "V",
            "address":"755 Ranger St. Oakdale, CA 95361",
            "squareFeet":1960,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot104: {
            "lotNumber":104,
            "phase": "V",
            "address":"737 Ranger St. Oakdale, CA 95361",
            "squareFeet":1754,
            "elevation":"monterey",
            "homeStyle":"single-story",
            "lotSize":"6384",
            "community":"Belmont"
        },
        lot105: 
        {"lotNumber":105,
         "phase": "V",
         "address":"719 Ranger St. Oakdale, CA 95361",
         "squareFeet":3041,
         "elevation":"spanish",
         "homeStyle":"two-story",
         "lotSize":"8751",
         "community":"Belmont"
        },
        lot106: {
            "lotNumber":106,
            "phase": "V",
            "address":"1279 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2609,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"8480",
            "community":"Belmont"
        },
        lot107: {
            "lotNumber":107,
            "phase": "V",
            "address":"1267 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2291,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"7544",
            "community":"Belmont"
        },
        lot108: {
            "lotNumber":108,
            "phase": "V",
            "address":"1255 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2291,
            "elevation":"spanish",
            "homeStyle":"single-story",
            "lotSize":"6721",
            "community":"Belmont"
        },
        lot109: {
            "lotNumber":109,
            "phase": "V",
            "address":"1243 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":1960,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"6572",
            "community":"Belmont"
        },
        lot110: {
            "lotNumber":110,
            "phase": "V",
            "address":"1231 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2209,
            "elevation":"monterey",
            "homeStyle":"two-story",
            "lotSize":"6141",
            "community":"Belmont"
        },
        lot111: {
            "lotNumber":111,
            "phase": "V",
            "address":"1219 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2609,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"9414",
            "community":"Belmont"
        },
        lot112: {
            "lotNumber":112,
            "phase": "V",
            "address":"1207 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2810,
            "elevation":"mediterranean",
            "homeStyle":"two-story",
            "lotSize":"8059",
            "community":"Belmont"
        },
        lot113: {
            "lotNumber":113,
            "phase": "V",
            "address":"1220 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2609,
            "elevation":"spanish",
            "homeStyle":"single-story",
            "lotSize":"12304",
            "community":"Belmont"
        },
        lot114: {
            "lotNumber":114,
            "phase": "V",
            "address":"1232 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":3041,
            "elevation":"craftsman",
            "homeStyle":"two-story",
            "lotSize":"7342",
            "community":"Belmont"
        },
        lot115: {
            "lotNumber":115,
            "phase": "V",
            "address":"1244 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2281,
            "elevation":"monterey",
            "homeStyle":"two-story",
            "lotSize":"6657",
            "community":"Belmont"
        },
        lot116: {
            "lotNumber":116,
            "phase": "V",
            "address":"1256 Bluegrass Ct. Oakdale, CA 95361",
            "squareFeet":2333,
            "elevation":"spanish",
            "homeStyle":"two-story",
            "lotSize":"8751",
            "community":"Belmont"
        },
        lot124: {
            "lotNumber":124,
            "phase": "V",
            "address":"613 Ranger St. Oakdale, CA 95361",
            "squareFeet":1919,
            "elevation":"monterey",
            "homeStyle":"single-story",
            "lotSize":"7552",
            "community":"Belmont"
        },
        lot129: {
            "lotNumber":129,
            "phase": "V",
            "address":"616 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":1754,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"7356",
            "community":"Belmont"
        },
        lot130: {
            "lotNumber":130,
            "phase": "V",
            "address":"628 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":2055,
            "elevation":"plantation",
            "homeStyle":"two-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot131: {
            "lotNumber":131,
            "phase": "V",
            "address":"636 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":1754,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot132: {
            "lotNumber":132,
            "phase": "V",
            "address":"648 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":1919,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"6000",
            "community":"Belmont"
        },
        lot133: {
            "lotNumber":133,
            "phase": "V",
            "address":"656 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":1960,
            "elevation":"spanish",
            "homeStyle":"single-story",
            "lotSize":"6224",
            "community":"Belmont"
        },
        lot134: {
            "lotNumber":134,
            "phase": "V",
            "address":"664 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":2609,
            "elevation":"craftsman",
            "homeStyle":"single-story",
            "lotSize":"6787",
            "community":"Belmont"
        },
        lot135: {
            "lotNumber":135,
            "phase": "V",
            "address":"676 Branding Iron St. Oakdale, CA 95361",
            "squareFeet":2291,
            "elevation":"mediterranean",
            "homeStyle":"single-story",
            "lotSize":"7306",
            "community":"Belmont"
        },
        lot136: {
            "lotNumber":136,
            "phase": "V",
            "address":"684Branding Iron St. Oakdale, CA 95361",
            "squareFeet":2609,
            "elevation":"monterey",
            "homeStyle":"single-story",
            "lotSize":"7819",
            "community":"Belmont"
        }
    }
     

    
};